import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  tables = [];
  constructor(private http: HttpClient) {

  }

  ngOnInit() {
    this.http.get('./assets/data.json').subscribe((data: any) => {
      console.log(data);
      this.tables = data;
    });
  }
}


